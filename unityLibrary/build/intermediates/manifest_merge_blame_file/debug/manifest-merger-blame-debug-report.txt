1<?xml version="1.0" encoding="utf-8"?>
2<!-- GENERATED BY UNITY. REMOVE THIS COMMENT TO PREVENT OVERWRITING WHEN EXPORTING AGAIN -->
3<manifest xmlns:android="http://schemas.android.com/apk/res/android"
4    xmlns:tools="http://schemas.android.com/tools"
5    package="com.unity3d.player"
6    android:versionCode="1"
7    android:versionName="1.0" >
8
9    <uses-sdk
10        android:minSdkVersion="19"
10-->D:\android\mistic-gems\unityLibrary\src\main\AndroidManifest.xml
11        android:targetSdkVersion="30" />
11-->D:\android\mistic-gems\unityLibrary\src\main\AndroidManifest.xml
12
13    <uses-feature android:glEsVersion="0x00020000" />
13-->D:\android\mistic-gems\unityLibrary\src\main\AndroidManifest.xml:14:3-52
13-->D:\android\mistic-gems\unityLibrary\src\main\AndroidManifest.xml:14:17-49
14    <uses-feature
14-->D:\android\mistic-gems\unityLibrary\src\main\AndroidManifest.xml:15:3-88
15        android:name="android.hardware.touchscreen"
15-->D:\android\mistic-gems\unityLibrary\src\main\AndroidManifest.xml:15:17-60
16        android:required="false" />
16-->D:\android\mistic-gems\unityLibrary\src\main\AndroidManifest.xml:15:61-85
17    <uses-feature
17-->D:\android\mistic-gems\unityLibrary\src\main\AndroidManifest.xml:16:3-99
18        android:name="android.hardware.touchscreen.multitouch"
18-->D:\android\mistic-gems\unityLibrary\src\main\AndroidManifest.xml:16:17-71
19        android:required="false" />
19-->D:\android\mistic-gems\unityLibrary\src\main\AndroidManifest.xml:16:72-96
20    <uses-feature
20-->D:\android\mistic-gems\unityLibrary\src\main\AndroidManifest.xml:17:3-108
21        android:name="android.hardware.touchscreen.multitouch.distinct"
21-->D:\android\mistic-gems\unityLibrary\src\main\AndroidManifest.xml:17:17-80
22        android:required="false" />
22-->D:\android\mistic-gems\unityLibrary\src\main\AndroidManifest.xml:17:81-105
23
24    <application android:isGame="true" >
24-->D:\android\mistic-gems\unityLibrary\src\main\AndroidManifest.xml:4:3-13:17
24-->D:\android\mistic-gems\unityLibrary\src\main\AndroidManifest.xml:4:16-37
25        <activity
25-->D:\android\mistic-gems\unityLibrary\src\main\AndroidManifest.xml:5:5-8:16
26            android:name="com.unity3d.player.UnityPlayerActivity"
26-->D:\android\mistic-gems\unityLibrary\src\main\AndroidManifest.xml:5:15-68
27            android:configChanges="mcc|mnc|locale|touchscreen|keyboard|keyboardHidden|navigation|orientation|screenLayout|uiMode|screenSize|smallestScreenSize|fontScale|layoutDirection|density"
27-->D:\android\mistic-gems\unityLibrary\src\main\AndroidManifest.xml:5:180-361
28            android:hardwareAccelerated="false"
28-->D:\android\mistic-gems\unityLibrary\src\main\AndroidManifest.xml:5:362-397
29            android:launchMode="singleTask"
29-->D:\android\mistic-gems\unityLibrary\src\main\AndroidManifest.xml:5:148-179
30            android:screenOrientation="portrait"
30-->D:\android\mistic-gems\unityLibrary\src\main\AndroidManifest.xml:5:111-147
31            android:theme="@style/UnityThemeSelector" >
31-->D:\android\mistic-gems\unityLibrary\src\main\AndroidManifest.xml:5:69-110
32            <meta-data
32-->D:\android\mistic-gems\unityLibrary\src\main\AndroidManifest.xml:6:7-82
33                android:name="unityplayer.UnityActivity"
33-->D:\android\mistic-gems\unityLibrary\src\main\AndroidManifest.xml:6:18-58
34                android:value="true" />
34-->D:\android\mistic-gems\unityLibrary\src\main\AndroidManifest.xml:6:59-79
35            <meta-data
35-->D:\android\mistic-gems\unityLibrary\src\main\AndroidManifest.xml:7:7-78
36                android:name="android.notch_support"
36-->D:\android\mistic-gems\unityLibrary\src\main\AndroidManifest.xml:7:18-54
37                android:value="true" />
37-->D:\android\mistic-gems\unityLibrary\src\main\AndroidManifest.xml:7:55-75
38        </activity>
39
40        <meta-data
40-->D:\android\mistic-gems\unityLibrary\src\main\AndroidManifest.xml:9:5-69
41            android:name="unity.splash-mode"
41-->D:\android\mistic-gems\unityLibrary\src\main\AndroidManifest.xml:9:16-48
42            android:value="0" />
42-->D:\android\mistic-gems\unityLibrary\src\main\AndroidManifest.xml:9:49-66
43        <meta-data
43-->D:\android\mistic-gems\unityLibrary\src\main\AndroidManifest.xml:10:5-74
44            android:name="unity.splash-enable"
44-->D:\android\mistic-gems\unityLibrary\src\main\AndroidManifest.xml:10:16-50
45            android:value="True" />
45-->D:\android\mistic-gems\unityLibrary\src\main\AndroidManifest.xml:10:51-71
46        <meta-data
46-->D:\android\mistic-gems\unityLibrary\src\main\AndroidManifest.xml:11:5-81
47            android:name="notch.config"
47-->D:\android\mistic-gems\unityLibrary\src\main\AndroidManifest.xml:11:16-43
48            android:value="portrait|landscape" />
48-->D:\android\mistic-gems\unityLibrary\src\main\AndroidManifest.xml:11:44-78
49        <meta-data
49-->D:\android\mistic-gems\unityLibrary\src\main\AndroidManifest.xml:12:5-101
50            android:name="unity.build-id"
50-->D:\android\mistic-gems\unityLibrary\src\main\AndroidManifest.xml:12:16-45
51            android:value="c7f5c310-9eb7-467c-a028-42f1735921d5" />
51-->D:\android\mistic-gems\unityLibrary\src\main\AndroidManifest.xml:12:46-98
52    </application>
53
54</manifest>
